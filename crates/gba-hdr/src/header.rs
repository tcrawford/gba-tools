// SPDX-License-Identifier: MPL-2.0
// SPDX-FileCopyrightText: 2022 Tim Crawford <crawfxrd@proton.me>

use std::{io, mem};

const HEADER_SIZE: usize = mem::size_of::<Header>();

const BRANCH_OPCODE: u32 = 0xEA00002E;

const NINTENDO_LOGO: [u8; 156] = [
    0x24, 0xFF, 0xAE, 0x51, 0x69, 0x9A, 0xA2, 0x21, 0x3D, 0x84, 0x82, 0x0A, 0x84, 0xE4, 0x09, 0xAD,
    0x11, 0x24, 0x8B, 0x98, 0xC0, 0x81, 0x7F, 0x21, 0xA3, 0x52, 0xBE, 0x19, 0x93, 0x09, 0xCE, 0x20,
    0x10, 0x46, 0x4A, 0x4A, 0xF8, 0x27, 0x31, 0xEC, 0x58, 0xC7, 0xE8, 0x33, 0x82, 0xE3, 0xCE, 0xBF,
    0x85, 0xF4, 0xDF, 0x94, 0xCE, 0x4B, 0x09, 0xC1, 0x94, 0x56, 0x8A, 0xC0, 0x13, 0x72, 0xA7, 0xFC,
    0x9F, 0x84, 0x4D, 0x73, 0xA3, 0xCA, 0x9A, 0x61, 0x58, 0x97, 0xA3, 0x27, 0xFC, 0x03, 0x98, 0x76,
    0x23, 0x1D, 0xC7, 0x61, 0x03, 0x04, 0xAE, 0x56, 0xBF, 0x38, 0x84, 0x00, 0x40, 0xA7, 0x0E, 0xFD,
    0xFF, 0x52, 0xFE, 0x03, 0x6F, 0x95, 0x30, 0xF1, 0x97, 0xFB, 0xC0, 0x85, 0x60, 0xD6, 0x80, 0x25,
    0xA9, 0x63, 0xBE, 0x03, 0x01, 0x4E, 0x38, 0xE2, 0xF9, 0xA2, 0x34, 0xFF, 0xBB, 0x3E, 0x03, 0x44,
    0x78, 0x00, 0x90, 0xCB, 0x88, 0x11, 0x3A, 0x94, 0x65, 0xC0, 0x7C, 0x63, 0x87, 0xF0, 0x3C, 0xAF,
    0xD6, 0x25, 0xE4, 0x8B, 0x38, 0x0A, 0xAC, 0x72, 0x21, 0xD4, 0xF8, 0x07,
];

const FIXED_CODE_BYTE: u8 = 0x96;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
#[repr(C)]
pub struct Header {
    pub jump: u32,
    pub logo: [u8; 156],
    pub title: [u8; 12],
    pub uttd: [u8; 4],
    pub maker: [u8; 2],
    pub fixed: u8,
    pub unit_code: u8,
    pub device_type: u8,
    pub reserved1: [u8; 7],
    pub version: u8,
    pub checksum: u8,
    pub reserved2: [u8; 2],
}

impl Default for Header {
    fn default() -> Self {
        Self {
            jump: BRANCH_OPCODE,
            logo: NINTENDO_LOGO,
            title: [0; 12],
            uttd: [b'B', 0, 0, b'E'],
            maker: [b'0', b'1'],
            fixed: FIXED_CODE_BYTE,
            unit_code: 0,
            device_type: 0,
            reserved1: [0; 7],
            version: 0,
            checksum: 0,
            reserved2: [0; 2],
        }
    }
}

impl Header {
    pub fn as_bytes(&self) -> &[u8] {
        // SAFETY: Safe to transmute to exact size.
        unsafe { mem::transmute::<&Header, &[u8; 192]>(self) }
    }

    pub fn update_checksum(&mut self) {
        let mut checksum = 0u8;

        let bytes = self.as_bytes();
        for b in &bytes[0xA0..0xBD] {
            checksum = checksum.wrapping_sub(*b);
        }

        checksum = checksum.wrapping_sub(0x19);
        self.checksum = checksum;
    }
}

impl From<[u8; HEADER_SIZE]> for Header {
    fn from(bytes: [u8; HEADER_SIZE]) -> Self {
        // SAFETY: Safe to transmute from exact size.
        unsafe { core::mem::transmute::<[u8; HEADER_SIZE], Header>(bytes) }
    }
}

impl TryFrom<&[u8]> for Header {
    type Error = io::Error;

    fn try_from(slice: &[u8]) -> Result<Self, Self::Error> {
        if slice.len() == HEADER_SIZE {
            let mut bytes = [0; HEADER_SIZE];
            bytes.copy_from_slice(slice);
            Ok(Header::from(bytes))
        } else {
            Err(io::Error::new(
                io::ErrorKind::InvalidInput,
                format!("Invalid header size of {} bytes", slice.len()),
            ))
        }
    }
}
