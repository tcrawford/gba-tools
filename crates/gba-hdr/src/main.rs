// SPDX-License-Identifier: MPL-2.0
// SPDX-FileCopyrightText: 2022 Tim Crawford <crawfxrd@proton.me>

mod cli;
mod header;

use std::io::{self, Read, Write};
use std::{fs, mem, path, str};

use clap::Parser;
use cli::{Cli, Command};
use header::Header;

fn read_header<P: AsRef<path::Path>>(path: P) -> io::Result<Header> {
    if (fs::metadata(&path)?.len() as usize) < mem::size_of::<Header>() {
        return Err(io::Error::new(
            io::ErrorKind::InvalidData,
            "File not large enough to contain GBA ROM header",
        ));
    }

    #[rustfmt::skip]
    let mut file = fs::OpenOptions::new()
        .read(true)
        .write(false)
        .create(false)
        .truncate(false)
        .open(path)?;

    let mut bytes = [0; mem::size_of::<Header>()];
    file.read_exact(&mut bytes)?;

    Ok(Header::from(bytes))
}

fn write_header<P: AsRef<path::Path>>(hdr: Header, path: P) -> io::Result<()> {
    if (fs::metadata(&path)?.len() as usize) < mem::size_of::<Header>() {
        return Err(io::Error::new(
            io::ErrorKind::InvalidData,
            "File not large enough to contain GBA ROM header",
        ));
    }

    #[rustfmt::skip]
    let mut file = fs::OpenOptions::new()
        .read(true)
        .write(true)
        .create(false)
        .truncate(false)
        .open(path)?;

    file.write_all(hdr.as_bytes())
}

fn main() -> io::Result<()> {
    let cli = Cli::parse();

    let curr = read_header(&cli.rom)?;

    match &cli.command {
        Command::Read => {
            let title = unsafe { str::from_utf8_unchecked(&curr.title) };
            let uttd = unsafe { str::from_utf8_unchecked(&curr.uttd) };
            let maker = unsafe { str::from_utf8_unchecked(&curr.maker) };

            println!("Title: \"{}\"", title);
            println!("UTTD: \"{}\"", uttd);
            println!("Maker: \"{}\"", maker);
            println!("Version: {}", curr.version);
        }
        Command::Write(args) => {
            let mut new = curr;

            if let Some(title) = &args.title {
                let title = title.to_ascii_uppercase();

                let mut slice = [0; 12];
                for (dest, src) in slice.iter_mut().zip(title.as_bytes().iter()) {
                    *dest = *src;
                }

                new.title = slice;
            }
            if let Some(uttd) = &args.code {
                new.uttd.copy_from_slice(uttd.as_bytes());
            }
            if let Some(maker) = &args.maker {
                new.maker.copy_from_slice(maker.as_bytes());
            }
            if let Some(version) = args.revision {
                new.version = version;
            }

            new.update_checksum();

            if new == curr {
                println!("New header is identical, not writing");
            } else {
                write_header(new, &cli.rom)?;
            }
        }
    }

    Ok(())
}
