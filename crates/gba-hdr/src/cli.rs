// SPDX-License-Identifier: MPL-2.0
// SPDX-FileCopyrightText: 2022 Tim Crawford <crawfxrd@proton.me>

use std::{io, path};

use clap::{Args, Parser, Subcommand};

fn validate_title(s: &str) -> io::Result<String> {
    if s.is_ascii() {
        Ok(s.to_string())
    } else {
        Err(io::Error::new(io::ErrorKind::InvalidInput, "Title must be an ASCII string"))
    }
}

fn validate_uttd(s: &str) -> io::Result<String> {
    let bytes = s.as_bytes();
    if bytes.len() == 4 && bytes.iter().all(|b| b.is_ascii_uppercase() || b.is_ascii_digit()) {
        Ok(s.to_string())
    } else {
        Err(io::Error::new(
            io::ErrorKind::InvalidInput,
            "Cartridge code must be a 4-byte uppercase ASCII string",
        ))
    }
}

fn validate_maker(s: &str) -> io::Result<String> {
    let bytes = s.as_bytes();
    if bytes.len() == 2 && bytes.iter().all(|b| b.is_ascii_uppercase() || b.is_ascii_digit()) {
        Ok(s.to_string())
    } else {
        Err(io::Error::new(
            io::ErrorKind::InvalidInput,
            "Maker code must be a 2-byte uppercase ASCII string",
        ))
    }
}

#[derive(Args)]
pub struct WriteArgs {
    /// Set the game title
    #[clap(short, long, value_parser = validate_title)]
    pub title: Option<String>,

    /// Set the UTTD game code
    #[clap(short, long, value_parser = validate_uttd)]
    pub code: Option<String>,

    /// Set the maker code
    #[clap(short, long, value_parser = validate_maker)]
    pub maker: Option<String>,

    /// Set the ROM version
    #[clap(short, long, value_name = "VERSION", value_parser)]
    pub revision: Option<u8>,
}

#[derive(Subcommand)]
pub enum Command {
    /// Read the header of a ROM file
    Read,
    /// Update the header of a ROM file
    Write(WriteArgs),
}

#[derive(Parser)]
#[clap(about, version)]
pub struct Cli {
    /// GBA ROM file
    #[clap(value_parser)]
    pub rom: path::PathBuf,

    #[clap(subcommand)]
    pub command: Command,
}
