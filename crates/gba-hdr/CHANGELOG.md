# Changelog

## 0.1.0 (unreleased)

- Implemented reading/writing title, UTTD, maker code, and version in header.
- Added CLI using clap-derive.
